import collections
# from concurrent.futures import thread

from flask import Flask, render_template, Response, request, session, jsonify
import cv2
import numpy as np
from gevent import monkey
monkey.patch_all()
import tensorflow.compat.v1 as tf
from gevent.pywsgi import WSGIServer
from object_detection.utils import label_map_util
# from object_detection.utils import visualization_utils as vis_util
from object_detection.utils.visualization_utils import STANDARD_COLORS, draw_bounding_box_on_image_array
import six
import time
from PIL import Image, ImageColor
from geventwebsocket.handler import WebSocketHandler
from socketio import Server, WSGIApp

PATH_TO_FROZEN_GRAPH = 'model/frozen_inference_graph.pb'

PATH_TO_LABEL_MAP = 'data/label_map.pbtxt'

NUM_CLASSES = 1


RESIZED_X = 1280
RESIZED_Y = 720
prev_dict = {}
jam_list = []
max_boxes_to_draw = 20

app = Flask(__name__, static_folder="static")
app.secret_key = "abc"
sio = Server(async_mode='gevent')
app.wsgi_app = WSGIApp(sio, app.wsgi_app)
thread = None


def background_thread():
    """Example of how to send server generated events to clients."""
    count = 0
    while True:
        sio.sleep(10)
        count += 1
        sio.emit('status', {'data': 'Server generated event'})


threshold = 0.2
line_thickness = 3
use_normalized_coordinates = True

prev_cent_x = []
prev_cent_y = []

detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

label_map = label_map_util.load_labelmap(PATH_TO_LABEL_MAP)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)


def jam_check(x, y, w, h, count):
    jam = False
    flag_val = False

    if count % 75 == 0 and count != 0:
        xmin, ymin, xmax, ymax = x, y, x + w, y + h
        cent_x = (xmin + xmax) / 2
        cent_y = (ymin + ymax) / 2
        centroids = [cent_x, cent_y]
        area = (xmax - xmin) * (ymax - ymin)
        dd = [d for d in prev_cent_x if d[0] <= centroids[0] + 0.05 and d[0] >= centroids[0] - 0.05 \
              and d[1] <= centroids[1] + 0.05 and d[1] >= centroids[1] - 0.05]
        if dd:
            for val in dd:
                if area <= prev_dict[tuple(val)] + 0.01 and area >= prev_dict[tuple(val)] - 0.01:
                    jam = True
                    prev_cent_y.append("yes")
                    print("Jammed object --> ", dd)
                else:
                    prev_dict[tuple(val)] = area

        else:
            prev_cent_y.append("no")
            prev_cent_x.append(centroids)
            prev_dict[tuple(centroids)] = area
            print(centroids)

    jam_list.append(jam)

    if len(jam_list) >= 10:
        if jam_list[-1] or jam_list[-2]:
            flag_val = True

    return flag_val

@sio.event
def generate_alert():
    # import pdb; pdb.set_trace()
    sio.emit('status', {'data': 'Jam Detected'})
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

def drawing(zoomed_frame, text, color, flag, flag_list, x, y):
    if flag:
        flag_list.append("yes")
    else:
        flag_list.append("no")

    if "yes" in set(flag_list):
        # cv2.putText(zoomed_frame, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.4, color, 1)
        # cv2.putText(zoomed_frame, "Alert: Jam Detected", (100, 100), cv2.LINE_AA, 1, color, 3)
        generate_alert()
        # socketio.on_event('Jam Detected', generate_alert, namespace='/jam_detected')
    else:
        cv2.putText(zoomed_frame, text, (int(x), int(y) - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.4, color, 1)

def detection(vs):
    frame_counter = 0
    # fps = str(vs.get(cv2.CAP_PROP_FPS))
    flag = False
    xmin, ymin = 0, 0
    flag_list = []
    # import pdb; pdb.set_trace()
    display_str = ''
    color = (0, 0, 0)

    with detection_graph.as_default():
        with tf.Session(graph=detection_graph) as sess:
            while vs.isOpened():
                # Read frame from camera
                ret, image_np = vs.read()
                # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
                image_np_expanded = np.expand_dims(image_np, axis=0)
                # Extract image tensor
                image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
                # Extract detection boxes
                boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
                # Extract detection scores
                scores = detection_graph.get_tensor_by_name('detection_scores:0')
                # Extract detection classes
                classes = detection_graph.get_tensor_by_name('detection_classes:0')
                # Extract number of detections
                num_detections = detection_graph.get_tensor_by_name(
                    'num_detections:0')
                # Actual detection.
                (boxes, scores, classes, num_detections) = sess.run(
                    [boxes, scores, classes, num_detections],
                    feed_dict={image_tensor: image_np_expanded})
                # Visualization of the results of a detection.
                boxes = np.squeeze(boxes)
                scores = np.squeeze(scores)
                classes = np.squeeze(classes).astype(np.int32)

                box_to_display_str_map = collections.defaultdict(list)
                box_to_color_map = collections.defaultdict(str)

                for i in range(min(max_boxes_to_draw, boxes.shape[0])):
                    if scores is None or scores[i] > threshold:
                        box = tuple(boxes[i].tolist())
                        if classes[i] in six.viewkeys(category_index):
                            class_name = category_index[classes[i]]['name']
                        display_str = str(class_name)
                        display_str = '{}: {}%'.format(display_str, int(100 * scores[i]))
                        box_to_display_str_map[box].append(display_str)
                        box_to_color_map[box] = STANDARD_COLORS[
                            classes[i] % len(STANDARD_COLORS)]
                for box,color in box_to_color_map.items():
                    ymin, xmin, ymax, xmax = box
                    flag = jam_check(xmin, ymin, xmax, ymax, frame_counter)
                    draw_bounding_box_on_image_array(
                        image_np,
                        ymin,
                        xmin,
                        ymax,
                        xmax,
                        color=color,
                        thickness=line_thickness,
                        display_str_list=box_to_display_str_map[box],
                        use_normalized_coordinates=use_normalized_coordinates)

                try:
                    color = ImageColor.getrgb(color)
                except Exception as e:
                    # import pdb; pdb.set_trace()
                    drawing(image_np, display_str, color, flag, flag_list, xmin, ymin)
                drawing(image_np, display_str, color, flag, flag_list, xmin, ymin)

                frame_counter += 1
                ret, jpeg = cv2.imencode('.jpg', image_np)
                frame = jpeg.tobytes()
                yield (b'--frame\r\n'
                       b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


@app.route("/")
def index():
    global thread
    if thread is None:
        thread = sio.start_background_task(background_thread)
    return render_template("index.html")

@app.route("/release")
def release():
    import pdb; pdb.set_trace()
    vs1.release()

@app.route("/run", methods=['GET', 'POST'])
def run():
    print(request.args.get('url'))
    vs_url = request.args.get('url')
    print(vs_url)
    vs1 = cv2.VideoCapture(vs_url)

    return Response(response=detection(vs1), mimetype='multipart/x-mixed-replace; boundary=frame')
    # return render_template("index.html")

http_server = WSGIServer(('0.0.0.0', 5000), app, handler_class=WebSocketHandler)
http_server.serve_forever()

